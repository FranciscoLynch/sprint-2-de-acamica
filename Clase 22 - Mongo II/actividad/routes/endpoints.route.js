'use strict';

const express = require('express'),
    router = express.Router(),
    usuario = require('../models/usuario.model'),
    mongoose = require('mongoose');

const assert = require('assert');
const usuarioModel = require('../models/usuario.model');

const Schema = require('../models/usuario.model');

const cuentaBanco = mongoose.model('Cuenta', usuarioModel)

router.get('/listar', (req, res) => {
    cuentaBanco.find().then((result, error) => {
        if (error) {
            res.json('Ha ocurrido un error');
            throw new Error('Ha ocurrido un error');
        } else {
            res.json(result);
        }
    }).catch((err) => {
        console.log(err);
        res.json('Ha ocurrido un error');
    });
});

router.post('/alta-usuario', (req, res) => {
    const { nombre, apellido, email } = req.body;

    let nuevo_usuario = new Usuario({
        nombre: nombre,
        apellido: apellido,
        email: email,
        saldo: 0
    });

    let dat = new cuentaBanco(nuevo_usuario);

    dat.save((error) => {
        if (error) {
            return res.json({
                success: false,
                msj: 'El usuario no se pudo registrar',
                err
            });
        } else {
            res.json({
                success: true,
                msj: 'El usuario se registro con exito'
            });
        }
    });

});

router.patch('/modificar-saldo/:email', (req, res) => {
    const { monto } = req.body;

    try {
        cuentaBanco.findOne({ email: req.params.email }).then((result, error) => {
            if (error) {
                throw new Error('Ha ocurrido un error');
            } else {
                result.saldo += monto;
                result.save((error) => {
                    if (error) {
                        res.json('Ha ocurrido un error', error);
                    } else {
                        res.json('Su saldo se acredito correctamente');
                    }
                });
            }
        });
    } catch (err) {
        res.json("HA OCURRIDO UN ERROR", error)
        console.log(err);
    }
});

router.put('/transferencia/:email', (req, res) => {
    const { emailDestino, monto } = req.body;

    (async function transferir() {
        const session = await mongoose.startSession();
        session.startTransaction();

        // verifico que exista la cuenta de destino
        let cuentaDest = await cuentaBanco.findOne({ email: emailDestino }).then((result) => {
            if (result) {
                console.log('Cuenta destino encontrada');
            } else {
                console.log('La cuenta ingresada no se encontro');
                session.abortTransaction();
            }
        }).catch(error => {
            console.log('Ha ocurrido un error - linea 95', error);
            session.abortTransaction();
        });

        // verifico que exista la cuenta de origen
        let cuentaOrigen = await cuentaBanco.findOne({ email: req.params.email }).then((result) => {
            // verifico que la cuenta de origen tenga fondos suficientes 
            if (result.saldo < monto) {
                console.log('El saldo es insuficiente');
                session.abortTransaction();
            }
        }).catch(error => {
            console.log('Ha ocurrido un error - linea 115', error);
            session.abortTransaction();
        });

        // resto el monto de la cuenta de origen 
        await cuentaOrigen.then((result) => {
            result.saldo = result.saldo - monto;
            result.save();
        }).catch(err => {
            console.log("Ha ocurrido un error - linea 119", err);
            session.abortTransaction();
        });

        await cuentaDest.then((result) => {
            result.saldo = result.saldo + monto;
            result.save(); 
            res.json('La transferencia se realizo exitosamente');
        }).catch(err => {
            console.log("Ha ocurrido un error - linea 132", err);
            session.abortTransaction();
        });

        session.commitTransaction();
        session.endSession();

    })();
});

module.exports = router;