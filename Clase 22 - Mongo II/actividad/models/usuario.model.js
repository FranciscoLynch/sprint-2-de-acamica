'use strict';

const mongoose = require('mongoose');

const usuario_schema = new mongoose.Schema({
    nombre: { type: String, unique: false },
    apellido: { type: String, unique: false },
    email: { type: String, unique: true },
    saldo: { type: Number, unique: false }
});

module.exports = mongoose.model('Usuario', usuario_schema, 'Usuarios');