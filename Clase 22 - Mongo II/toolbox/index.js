/*  

Por un lado, tenemos métodos para gestionar la sesión:
    startSession(): las transacciones están asociadas a una sesión, es por ello que siempre necesitaremos abrir una sesión. 

    endSession(): cuando cerremos la sesión, si hay alguna transacción ejecutándose, esta se abortará.

Por el otro lado, encontramos los métodos para controlar la transacción: 
    Session.startTransaction(): este método nos sirve para empezar la transacción dentro de una sesión.

    Session.commitTransaction(): con este método confirmaremos los cambios que hayamos realizado dentro de la transacción. 

    Session.abortTransaction(): aborta la ejecución de la transacción y, por lo tanto, no graba las modificaciones hechas a lo largo de la transacción. 


*/  

// COMMIT 

const Usuario = db.model('User', new Schema({nombre: String})); 

const session = await db.startSession(); 
session.startTransaction(); 

// El metodo `create()` es parte de la transaccion por la opcion session 
await Usuario.create([{nombre: 'Test'}], { session: session}); 

// El documento no se va a ver hasta que se haga el commit 
let doc = await Usuario.findOne({nombre: 'Test'}); 
assert.ok(!doc); 

// Esta operacion va a devolver el documento ya que es parte de la transaccion
// con la opcion session 

doc = await Usuario.findOne({nombre: 'Test'}).session(session); 
assert.ok(doc); 
 
// Una vez que se commitea la transaccion el documento es visible fuerra de ella

await session.commitTransaction(); 
doc = await Usuario.findOne({nombre: 'Test'}); 
assert.ok(doc); 

session.endSession(); 
 

// ABORT 

const session = await db.startSession(); 
session.startTransaction();  

// Se crean dos usuarios dentro de esta transaccion
await Usuario.create([{nombre:'Test'}], { session: session}); 
await Usuario.create([{nombre:'Test2'}], { session: session}); 

// Al indicarle que aborte 
await session.abortTransaction(); 

// Ningun documento persiste en la BD 
const count = await Usuario.countDocuments(); 
// Asi que la cantidad es 0 
assert.stricEqual(count, 0); 

session.endSession();

