const redis = require('redis');  
const express = require('express'); 
const app = express(); 

const client = redis.createClient({ 
    host: '127.0.0.1', 
    port: 6379
}); 

client.on('error', function(error) {
    console.error(error);
}); 

// SET - SE UTILIZA PARA GUARDAR Y RECUPERAR INFORMACION 
// Cuando hagas el set, se guardará en la memoria una referencia, “key”, que va a tener el valor, “value”. 
let all_starks = await user.findAll({
    include: {
        model: House, 
        where: { 
            nombre: 'Stark'
        }
    }
}); 

client.set('starks', all_starks); 

// GET - SE UTILIZA PARA GUARDAR Y RECUPERAR INFORMACION 
// Al guardar el get, se indica el nombre de alguna clave que se haya guardado en memoria. Si esta existe, entonces se devolvera el valor de la misma.
client.get('starks', (error, rep) => {
    if(error) { 
        // hubo un error
        return;
    } 
    if (rep) { 
        // existe la clave starks 
        console.log(rep); // muestro su valor
    }
}); 

// EJEMPLO DE ENDPOINT 
app.get('/api/usuarios', async (req, res) => {
    // obtengo todos los usuarios 
    // me fijo si existen los usuarios en cache 
    client.get('usuarios', (error, rep) => {
        if(error){
            // hubo un error
            res.json(error);
        }
        if (rep) {
            // existe la clave, devuelvo los usuarios guardados en cache 
            res.json(rep);
        } 
    }); 
    // si no estan, los busco en la base de datos 
    const usuarios = await user.findAll(); 
    // y los guardo para futura consulta, dentro de la cache 
    client.set('usuarios', usuarios); 
    // devuelvo los usuarios 
    res.json(usuarios);
});

// SE LE INDICA TIEMPO DE VENCIMIENTO AL SET 

app.get('/api/usuarios', async (req, res) => {
    // obtengo todos los usuarios 
    // me fijo si existen los usuarios en cache 
    client.get('usuarios', (error, rep) => {
        if(error){
            // hubo un error
            res.json(error);
        }
        if (rep) {
            // existe la clave, devuelvo los usuarios guardados en cache 
            res.json(rep);
        } 
    }); 
    // si no estan, los busco en la base de datos 
    const usuarios = await user.findAll(); 
    // y los guardo para futura consulta, dentro de la cache 
    client.set('usuarios', JSON.stringify(usuarios), 'EX', 10*60*60, (error) => {
        // 'EX': expire en segundos 
        // cache por 1 hora 
        if (error) { 
            return res.json(error);
        }
    });   
    // devuelvo los usuarios 
    res.json(usuarios);
});