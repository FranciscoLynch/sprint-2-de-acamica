const redis = require('redis');
// crea la conexion con el servidor de redis 
const client = redis.createClient({
    host: 'localhost',
    port: 6379
});

// muestra un mensaje si ocurre un error en la conexion 
client.on('error', function (err) {
    console.log(err);
});
// muestra un mensaje si se pudo conectar 
client.on('connect', function () {
    console.log('conectado a redis!');
});

// tipos de datos en redis 

// STRINGS
// HASHES (ALMACENADO DE OBJETOS) 
// LIST (ALMACENADO LISTA DE UN ITEM)
// SETS (SIMILARES A LAS LISTAS PERO NO GUARDA REPETIDOS)
//  
//  OPERACIONES EN REDIS 

// exist() 
// del() 
// set() 
// expire() 
// hmset() 
// hgetall() 
// get() 
// lrange() 
// sadd() 
// smembers()

// Ejemplo de tipos de datos 

// KEY, VALUE 

client.set('framework', 'ReactJS');
client.set('database', 'MongoDB');
client.set('framework', 'ReactJS');
client.set('teacher', 'SteveJobs');

// consultar los valores usando get 
client.get('framework', (err, data) => {
    console.log('framework: ' + data);
});

client.get('teacher', (err, data) => {
    console.log(data);
});

// HASHES, guarda los valores (llave, valor) en objetos de  

client.hmset('tecnologias_hash', 'cache', 'Redis', 'nonrelational', 'MongoDB', 'relactionaldb', 'MySQL');

// usamos hgetall() para devolver todos los valores almacenados del hash 

// Redis hashes are maps 
client.hgetall('tecnologias_hash', (err, datatec) => {
    console.log(datatec);
});

// para sacar la informacion de un objeto 

client.hmget('tecnologias_hash', 'cache', (err, data) => {
    console.log('data del objeto cache: ', data);
});

// nueva version usando hset 

client.hset('hash key', 'hashes 1', 'some value', redis.print);
client.hset('hash key', 'hashes 2', 'some other value', redis.print);

// se puede hacer lo mismo para almacenar objetos 

client.hmset('tecnologias_front', {
    'javascript': 'ReactJS',
    'css': 'Boostrap',
    'node': 'Express'
});

// ***LIST 

// Permite almacenar una lista de items y devuelve en formato de array de  

client.rpush('framework_list', 'ReactJS', 'Angular', 'Ember', 'Vue', 'Laravel', '.NET CORE');

// visualizar los valores se usa el metodo lrange 

client.lrange('framework_list', 0, -1, (err, list) => {
    console.log(list);
});

// client.del('framework_list', (err, data) => {
//  console.log(data);
// }); 

// *** SETS */ 

// IGUAL QUE LAS LISTAS PERO NO PERMITE VALORES DUPLICADOS 

client.sadd('menu_restaurante', ['Botella de agua', 'Ensalada', 'Papitas', 'Pizza', 'Pizza', 'Hamburguesa', 'Ensalada'], (err, menudata) => {
    console.log(menudata);
}); 

// obtener los valores del set se usa el metodo smembers 

client.smembers('menu_restaurante', (err, menu) => {
    console.log({
        'menu': menu
    }); 
    console.log(menu); 
}); 

// **** VALIDANDO LLAVES */ 

// exist 
client.exists('framework', (err, data) => {
    if (data === 1) {
        console.log('Existe la llave framework'); 
    } else {
        console.log('No existe la llave');
    }
}); 

// delete 

client.del('framework', (err, data) => {
    console.log(data); // si devuelve 1 es por lo elimino
}); 

// exist 
client.exists('framework', (err, data) => {
    if (data === 1) {
        console.log('Existe la llave framework'); 
    } else {
        console.log('No existe la llave');
    }
});  

// incrementar valor usando incr 

client.set('dias_vacaciones', 20,() => {
    client.incr('dias_vacaciones', (err, data) => {
        console.log('dias de vacaciones aumentado a ', data);
    });
});