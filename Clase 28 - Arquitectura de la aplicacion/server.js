const express = require('express'),
    app = express(); 

app.use(express.json()); 
app.use(express.urlencoded({extended: true})); 

require('./src/database/db');
require('./src/database/sync');

const apiRouter = require('./src/routes/api'); 

app.use('/api', apiRouter);

app.listen(3000, () => console.log('Servidor ejecutandose en el puerto 3000'));