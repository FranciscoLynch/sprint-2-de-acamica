require('dotenv').config(); 

module.exports = { 

    database: {
        database: process.env.MYSQL_DATABASE, 
        username: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD, 
        host: process.env.MYSQL_HOST, 
        dialect: process.env.MYSQL_DIALECT
    }

}