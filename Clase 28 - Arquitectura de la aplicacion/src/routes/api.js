const router = require('express').Router(); 

const apiFruta = require('./api/frutas'); 

router.use('/frutas', apiFruta); 

module.exports = router;