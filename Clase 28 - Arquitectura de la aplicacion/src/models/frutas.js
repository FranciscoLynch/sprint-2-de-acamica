module.exports = (sequelize, type) => {

    return sequelize.define(
        "frutas", 
        { 
            nombre: type.STRING, 
            color: type.STRING, 
            vitaminas: type.STRING
        }, 
        { 
            sequelize, 
            timestamps: false
        }
    );
};