require('dotenv').config()

const { Sequelize } = require('sequelize')
const { database } = require('../config')

const dataBaseAcamicadb = new Sequelize(database)

async function validar_conexion() {
    try {
        await dataBaseAcamicadb.authenticate();
        console.log('Conexion ha sido establecida correctamente')
    } catch (error) {
        console.log('Error al conectarse a la base de datos', error)
    }
}

module.exports = {
    dataBaseAcamicadb
}
