const { Sequelize } = require('sequelize'); 
const { dataBaseAcamicadb } = require('./db'); 
const frutaModelo = require('../models/frutas'); 

const Frutas = frutaModelo(dataBaseAcamicadb, Sequelize); 

dataBaseAcamicadb.sync({force: false}).then(() => {
    console.log('Tablas sincronizadas');
}); 

module.exports = {
    Frutas
}