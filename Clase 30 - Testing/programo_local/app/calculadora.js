const sumar = function(a,b) {
    return a + b;
}; 

const resta = function(a,b) {
    return a - b;
}; 

const multiplicar = function(a,b) {
    return a * b;
}; 

const dividir = function(a,b) {
    return a / b;
};  

const mensaje = function(a) {
    return `El resultado es: ${a}`
};   

module.exports = {
    sumar, 
    resta, 
    multiplicar, 
    dividir, 
    mensaje
};
