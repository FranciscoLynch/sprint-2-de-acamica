const assert = require('assert');

describe('#test', function () {
    describe('#indexOf()', function () {
        it('deberia retornar -1 cuando el valor no esta en el array', function () {
            assert.strictEqual([1, 2, 3].indexOf(5), -1);
        })
    })
});

describe('Calculos aritmeticos', () => {
    before(() => {
        //inicializar base de datos, variables... 
        console.log('Este codigo se ejecuta antes de cualquier tester');
    });

    after(() => {
        // limpiar base de datos, tablas, arrays o variables 
        console.log('Este codigo se ejecuta despues de todos los test');
    });

    // describe permite agrupar pruebas similares en el mismo bloquear 
    describe('Suma', () => {
        // inicializar variable suma 
        let suma = 0;
        beforeEach(() => {
            console.log('Asignar valor inicial a la variable');
            suma = 2;
        });

        it('Deberia ser 2 + 3 y dar 5', () => {
            suma += 3;
            // compara valor actual mas esperado
            assert.strictEqual(suma, 5);
        });
    });

    describe('Resta', () => {
        // inicializar variable suma 
        let resta = 10;
        beforeEach(() => {
            console.log('Asignar valor inicial a la variable')
        });

        it('Le resto 3, deberia dar 7', () => {
            resta -= 3;
            // compara valor actual mas esperado 
            assert.strictEqual(resta, 7);
        });

        it('La variable resta debe ser 4', () => {
            resta -= 3;
            // compara valor actual mas esperado 
            assert.strictEqual(resta, 4);
        });
    });

});
