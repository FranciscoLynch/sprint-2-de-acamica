const express = require('express'); 
const app = express(); 
const jwt = require('jsonwebtoken');
 

const informacion = { 
    nombre: 'Hamburguesa doble con queso',
    precio: 123, 
    id: 1
}

const firma = 'clave123'; 

// firmar 
const token = jwt.sign(informacion, firma); 
console.log(token); 

// decodificar 
const descodificar = jwt.decode(token, firma); 
console.log(descodificar); 

app.use(express.json()); 


app.listen(4000, () => console.log('SERVIDOR CORRIENDO'));