const express = require('express');
const app = express(); 
const helmet = require('helmet'); 
const jwt = require('jsonwebtoken'); 

const informacion = { 
    nombre: 'Maria la del barrio', 
    id: 1
}

const firma = 'estaClaveSecreta'; 

// firmar 
const token = jwt.sign(informacion,firma); 
console.log(token); 
// decodificar 
const descodificar = jwt.decode(token, firma); 
console.log(descodificar); 

app.use(express.json()); 


// agregar seguridad a nuestro servidor usando helmet 
app.use(helmet()); 
app.post('/login', (req,res) => {
    const { usuario, contraseña }  = req.body; 
    const valido = validarUsuario(usuario, contraseña); 

    if (!valido) { 
        res.json({error: 'Usuario con contraseña incorrecta'}); 
        return; 
    }
    
    const token = jwt.sign({usuario}, firma); 

    res.json({token}); 
}); 

const autenticarUsuario = (req,res,next) => {
    try { 
        const token = req.headers.authorization.split('')[1]; 
        console.log('token desde postman headers', token); 
        const verificarToken = jwt.verify(token, firma); 
        if (verificarToken){ 
            req.usuario = verificarToken; 
            return next();
        } else {
            res.send('error en el login');
        } 
    } catch (error) {
        res.json({error_m: 'Error al validar'});
    }
}; 

const validarUsuario = (usuario, contraseña) => {
    try { 
        const claves = [ 
            {
                usuario: 'Luis', 
                contraseña: '123'
            }, 
            {
                usuario: 'Maria',
                contraseña: '123'
            }
        ]; 

        const valido = claves.filter(x => x.contraseña === contraseña && x.usuario === usuario); 
        if (valido.length > 0) { 
            return true; 
        } else {
            return false; 
        } 
    } catch (error) {
        res.send(error);
    }
}; 

app.post('/seguro', autenticarUsuario,(req, res) => {
    res.send(`Esta es una pagina autenticada ${req.usuario.usuario} `);
}); 

app.listen(3000, () => console.log('Servidor Corriendo'));