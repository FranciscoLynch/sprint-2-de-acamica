const router = require('express').Router();
const { Pelicula } = require('../../db');

router.get('/hola', (req, res) => {
    res.send('Hola peliculas');
});

// listado de peliculas /api/peliculas
router.get('/', async (req, res) => {
    const peliculas = await Pelicula.findAll();
    res.json(peliculas);
}); 

// crear peliculas 
router.post('/', async (req,res) => { 
    const pelicula = await Pelicula.create(req.body); 
    res.json(pelicula); 
}); 

// actualizar la pelicula 
router.put('/:peliculaid', async (req,res) => { 
    await Pelicula 
}); 

// Eliminar la pelicula 
router.delete('/:peliculaid', async (req,res) => {
    
});



module.exports = router;