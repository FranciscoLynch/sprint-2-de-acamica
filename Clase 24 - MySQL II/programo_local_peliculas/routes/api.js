const router = require('express').Router();
const apiPeliculas = require('./api/peliculas');  

router.use('/peliculas', apiPeliculas); 

module.exports = router;