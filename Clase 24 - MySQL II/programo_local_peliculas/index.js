const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// llamamos a la base de datos configurada para que construya las tablas 
require('./db'); 

// Configuramos nuestras rutas 
const apiRouter = require('./routes/api');

// instalamos los drivers de mysql, npm i mysql2 

// todas las rutas que vengan para /api, deben 
// por el archivo api.js 

app.use('/api', apiRouter); 






app.listen(3000, () => {
    console.log('Servidor ejecutandose por el puerto 3000');
});