const { Sequelize } = require('sequelize');
const PeliculaModelo = require('./models/pelicula');

// Option 2: configuracion de ase de datos sequelize 
const sequelize = new Sequelize('acamicadb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

// validar_conexion(); 

const Pelicula = PeliculaModelo(sequelize, Sequelize);

sequelize.sync({ force: false }).then(() => {
    console.log('Tablas sincronizadas');
});

async function validar_conexion() {
    try {
        await sequelize.authenticate();
        console.log('Conexion ha sido establecida correctamente');
    } catch (error) {
        console.log('Error al conectarse a la base de datos');
    }
}


module.exports = {
    Pelicula,
};