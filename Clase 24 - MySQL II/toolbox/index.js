const Sequelize = require("sequelize"); 
const sequelize = new Sequelize(''); 

// Ejecutar la sentencia SQL. El método query retorna una promesa; entonces la capturamos con .then 
sequelize.query('SELECT * FROM razas', 
    { type: sequelize.QueryTypes.SELECT }
).then(function (resultados) { 
    console.log(resultados);
});  

// Con Select vamos a realizar los reemplazos necesarios. Estos pueden realizarse de dos maneras: a través del signo ? o :campo

// El signo ? será reemplazado por el valor del array.
sequelize.query('SELECT * FROM tabla WHERE estado = ?', 
    { replacements: ['activo'], type: sequelize.QueryTypes.SELECT }
).then(projects => { 
    console.log(projects);
}); 

// :campo buscará la key que contenga el mismo nombre.
sequelize.query('SELECT * FROM tabla WHERE estado = :estado', 
    { replacements: ['activo'], type: sequelize.QueryTypes.SELECT }
).then(projects => { 
    console.log(projects);
}); 