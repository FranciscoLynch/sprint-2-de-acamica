const express = require('express');
const app = express();  

// call the database configured to build the tables 
require('./db');  

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// import the three files(albums, bands y songs) through the api.js file
const apiRouter = require('./routes/api');

// all the routes came for /api, they need to be managed by the api.js file
app.use('/api', apiRouter);

// start the server 
app.listen(5000, () => {
    console.log('SERVER ON PORT 5000');
}); 

