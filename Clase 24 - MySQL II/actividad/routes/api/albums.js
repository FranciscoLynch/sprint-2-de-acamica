const router = require('express').Router(),
    db = require('../../db'),
    m = require('../../middlewares/index');

router.get('/extract', async (req, res) => {
    const albums = await db.album.findAll();
    res.json(albums);
});

router.post('/insert', async (req, res) => {
    const album = await db.album.create(req.body);
    res.json(album);
});

router.put('/update/:albumId', async (req, res) => {
    await db.album.update(req.body, {
        where: {
            id: req.params.albumId
        }
    });
    res.json('Album modified');
});

router.delete('/remove/:albumId', async (req, res) => {
    await db.album.destroy({
        where: {
            id: req.params.albumId
        }
    })
        .then(function (x) {
            if (x === 1) {
                res.status(200).json({ message: 'Succesfully removed' });
            } else {
                res.status(404).json({ message: 'Album not found' });
            }
        })
        .catch(function (error) {
            res.status(500).json(error);
        });
});

module.exports = router;