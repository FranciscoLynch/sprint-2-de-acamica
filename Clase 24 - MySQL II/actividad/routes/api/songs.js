const router = require('express').Router(),
    db = require('../../db'),
    m = require('../../middlewares/index');

router.get('/extract', async (req, res) => {
    const songs = await db.song.findAll();
    res.json(songs);
});

router.post('/insert', async (req, res) => {
    const song = await db.song.create(req.body);
    res.json(song);
});

router.put('/update/:songId', async (req, res) => {
    await db.song.update(req.body, {
        where: {
            id: req.params.songId
        }
    });
    res.json('Song modified');
});

router.delete('/remove/:songId', async (req, res) => {
    await db.song.destroy({
        where: {
            id: req.params.songId
        }
    })
        .then(function (x) {
            if (x === 1) {
                res.status(200).json({ message: 'Succesfully removed' });
            } else {
                res.status(404).json({ message: 'Song not found' });
            }
        })
        .catch(function (error) {
            res.status(500).json(error);
        });
});

module.exports = router;