const router = require('express').Router(),
    db = require('../../db'),
    m = require('../../middlewares/index');

    router.get('/extract', async (req, res) => {
        const band = await db.band.findAll();
        res.json(band);
    });
    
    router.post('/insert', async (req, res) => {
        const band = await db.band.create(req.body);
        res.json(band);
    });
    
    router.put('/update/:bandId', async (req, res) => {
        await db.band.update(req.body, {
            where: {
                id: req.params.bandId
            }
        });
        res.json('Band modified');
    });
    
    router.delete('/remove/:bandId', async (req, res) => {
        await db.band.destroy({
            where: {
                id: req.params.bandId
            }
        })
            .then(function (x) {
                if (x === 1) {
                    res.status(200).json({ message: 'Succesfully removed' });
                } else {
                    res.status(404).json({ message: 'Band not found' });
                }
            })
            .catch(function (error) {
                res.status(500).json(error);
            });
    });
    

module.exports = router;