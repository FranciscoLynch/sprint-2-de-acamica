const router = require('express').Router(); 

const apiAlbums = require('./api/albums');
const apiBands = require('./api/bands');
const apiSongs = require('./api/songs'); 

router.use('/albums', apiAlbums);
router.use('/bands', apiBands);
router.use('/songs', apiSongs); 

module.exports = router;