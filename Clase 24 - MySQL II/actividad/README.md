Actividad final de la clase 24 - MySQL II 

1- npm init 

2- npm i express nodemon mysql2 sequelize  

3- creo 4 carpetas, middlewares, models, routes y views.

4- creo un archivo llamado index.js fuera de esas carpetas

5- configuro el archivo con express, 
const express = require('express');
const app = express(); 

app.use(express.json());
app.use(express.urlencoded({extended:true})); 

app.listen(app.get('port'), () => {
    console.log('SERVER ON');
}); 

6- en la carpeta models creo 3 archivos js con los respectivos modelos de cada tabla 

7- exporto los modelos y se importan en el siguiente

8- fuera de las carpetas creo un archivo llamado db.js, va a ser donde se configure y se sincronice la base de datos. 
Exporto las instancias creadas.

9- voy al index.js y llamo a la base de datos (require('./db')) para que construya las tablas 

10- en la carpeta routes creo un archivo llamado api.js y una carpeta llamada api. En la carpeta va a haber 3 archivos con el respectivo CRUD para cada archivo(tabla). Y en el archivo api.js es donde se van a importar esos 3 archivos y de ahi se van a exportar al index.js ¿Por qué se hace eso? Es para juntarlos y que al exportarlos al index.js, la app use la ruta /api para todos los archivos, ademas de que se ahorran lineas de codigo en el index si los importaran por separado.

11- importo en el index.js el archivo api.js