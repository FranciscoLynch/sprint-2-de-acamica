module.exports = (sequelize, type) => { 
    
    return sequelize.define('albums', {
        id:{ 
            type: type.INTEGER, 
             primaryKey: true, 
             autoIncrement: true, 
        }, 
        nombre_album: type.VARCHAR, 
        banda: { 
            type: type.INTEGER, 
            references: { 
                model: 'Bandas', 
                key: 'id'
            }
        },         // banda id
        fecha_publicacion: type.DATE
    });
}  




