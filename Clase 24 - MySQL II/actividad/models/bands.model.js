module.exports = (sequelize, type) => { 
    
    return sequelize.define('bands', {
        id:{ 
            type: type.INTEGER, 
             primaryKey: true, 
             autoIncrement: true, 
        }, 
        nombre: type.VARCHAR, 
        integrantes: type.INTEGER,
        fecha_inicio: type.DATE,
        fecha_separacion: type.DATE,
        pais: type.VARCHAR
    });
}