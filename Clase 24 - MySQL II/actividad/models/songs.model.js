module.exports = (sequelize, type) => { 
    
    return sequelize.define('songs', {
        id:{ 
            type: type.INTEGER, 
             primaryKey: true, 
             autoIncrement: true, 
        }, 
        nombre: type.VARCHAR, 
        duracion: type.INTEGER, 
        album: { 
            type: type.INTEGER, 
            references: { 
                model: 'Albums', 
                key: 'id'
            }
        },          // album id
        banda: { 
            type: type.INTEGER, 
            references: { 
                model: 'Bandas', 
                key: 'id'
            }
        },          // banda id
        fecha_publicacion: type.DATE
    });
}