const mysql = require('mysql2');
const { Sequelize } = require('sequelize');

const albums = require('./models/albums.model'),
    bands = require('./models/bands.model'),
    songs = require('./models/songs.model');

// config
const sequelize = new Sequelize('database', 'username', 'password', {
    host: 'localhost',
    dialect: 'mysql',
});

// testing the connection
(async function test() {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established succesfully');
    } catch (error) {
        console.error('Unable to connect to the database');
    }
})();

// Close the connection => sequelize.close() 

// create an instance of each model to be exported and used for others files
const album = albums(sequelize, Sequelize);
const band = bands(sequelize, Sequelize);
const song = songs(sequelize, Sequelize);


sequelize.sync({ force: false }).then(() => {
    console.log('The tables were synchronized');
});

module.exports = {
    album,
    band,
    song
};