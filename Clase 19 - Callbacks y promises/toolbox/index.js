// promesa 

let mi_promesa = new Promise((resolve, reject) => {
    const number = Math.floor(Math.random() * 5);
  if(number > 0){
    resolve((number % 2)? "es impar" : "es par");
  }else{
    reject("es cero")
  }
}); 

mi_promesa
    .then(number => console.log(number))
    .catch(error => console.error(error));