const fetch = require('node-fetch'); 

function getGitHubUser(username){ 
    fetch('https://api.github.com/users/' + username)
    .then(response => response.json())
    .then(json => { 
        console.log(json); 
        getUserFirst5Followers(json.followers_url);
    }).catch(err => { 
        console.error('fetch failed', err);
    });
} 

function getUserFirst5Followers(url){ 
    fetch(url)
        .then(response => response.json())
        .then(jsondata => { 
            console.log(jsondata); 
            for (let index = 0; index < 5; index++) {
                console.log(jsondata[index].login);
            }
        }).catch(err => { 
            console.log('fetch failed', err);
        })
} 

getGitHubUser('andrew');