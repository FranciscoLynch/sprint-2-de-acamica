const promesa_numero = new Promise(function (resolve, reject) {
    const numero = Math.floor(Math.random() * 10);
    setTimeout(() => {
        numero > 4 ? resolve(numero) : reject(new Error('Menor a 4 : ' + numero));
    }, 2000);
}); 

promesa_numero
    .then(numero => console.log(numero))
    .catch(error => console.log(error)); 

function metodo_sincronico(){
    console.log("me gusta");
    console.log("programar");
    console.log("con cerveza");
    console.log("todos los dias");
    console.log("con mis perros");
    console.log("y gatos");
}

function metodo_asincronico(){
    console.log("NO me gustan");
    console.log("LOS DIAS SIN PROGRAMAR");
    setTimeout(() => {
        console.log("con AGUA");
    }, 1000);
    
    setTimeout(() => {
        console.log("SIN cerveza");
    }, 1000);
    
    setTimeout(() => {
        console.log("SOLO LOS VIERNES");
    }, 2000);
    
    console.log("con mis perros");
    console.log("y gatos");
}

// promesa 

let promesa_ejecucion = new Promise((resolve, reject) => {
     
    setTimeout(() => {
        resolve('Promesa resuelta en 3 segundos');
    }, 1000); 
    
    setTimeout(() => {
        reject('Promesa rechazada');
    }, 1500); 

}); 

// atrapamos la promesa usando then y catch 

promesa_ejecucion.then((mensaje) => { 
    console.time("execution");
    console.log('Respuesta es ' + mensaje);
    console.timeEnd('execution');
}).catch((errorMesagge) => { 
    console.log('Error de promesa ' + errorMesagge);
}); 

// haciendo que entre en el rejecting 

let promesa_rechazada = new Promise((resolve, reject) => { 

    setTimeout(() => {
        resolve('Promesa ejecutada correctamente, seguro?');
    }, 1000);

    setTimeout(() => {
        reject('Promesa rechazada');
    }, 500);
}); 

// ejemplo de promesa pendiente que termine de resolverse 

let promesa_pendiente = new Promise((resolve, reject) => { 
    console.log('pending...!');
    setTimeout(() => {
        if (!true){
            resolve('Promesa resuelta!');
        } else { 
            reject('Promesa rechazada');
        }
    }, 3500);
}); 

promesa_pendiente.then((successMessage) => {
    console.log('Respuesta de promesa pendiente: ' + successMessage);
}).catch((errorMesagge) => {
    console.log('¡Error de promesa pendiente! ' + errorMesagge);
});

// ejemplo de fetch con promesa para obtener una imagen 

let divImage = document.getElementById('imgDog'); 

function getDogImage(url) {
    console.log('Pendiente para la imagen'); 
    fetch(url)
        .then(response => response.json())
        .then(json => { 

            let dogImage = document.createElement('img');
            dogImage.setAttribute('src', json.message);
            dogImage.style.width = '300px';
            divImage.appendChild(dogImage);
            console.log('Imagen cargada');
        }) 
        .catch(err => { 
            console.log('Fallo la comunicacion al server');
        }); 
} 
d
getDogImage("https://dog.ceo/api/breeds/image/random");
metodo_sincronico();
metodo_asincronico();