/*
    Cómo crear una base de datos relacional

Planifica el tipo de información que se necesita almacenar, teniendo en cuenta la información disponible y la que necesitamos.
Crea una tabla por cada entidad que tengas en el sistema.
Entendemos por entidad a un objeto que vamos a modelar, del cual necesitamos sus atributos.
Para cada campo es necesario determinar los tipos de datos que va a contener, aglomerados en 3 grandes grupos: numéricos, fecha y cadena.

Tipos numéricos

Tipos de Fecha

Tipos de Cadena


Otro concepto importante es el de clave primaria: es una columna especial que permite identificar cada uno de los registros de forma única.
Para evitar que haya datos repetidos, agregamos la columna adicional ID al inicio de cada tabla y le asignamos la propiedad auto_increment.
Así, se le indica al motor de la base de datos que tiene que aumentar ese valor de forma consecutiva cada vez que se agregue un nuevo registro.


Los 7 comandos más utilizados en SQL

Dos categorías: los que pertenecen al lenguaje de definición de datos y los que pertenecen a la manipulación de datos:

1. Definición de datos.
CREATE TABLE. Permite crear tablas. para eso, debes definir un nombre y los campos junto con su tipo.

CREATE TABLE `usuarios` (
  `nombre` varchar(100),
  `apellido` varchar(200),
  `edad` int(5)
    );


ALTER TABLE. Permite modificar la estructura de una tabla. 
En el siguiente ejemplo, ha cambiado el campo de nombre a nombre_completo. ¡Esta operación no modificará ningún registro!

ALTER TABLE usuarios CHANGE nombre nombre_completo VARCHAR(100);


DROP TABLE. Esta instrucción elimina la tabla junto con los campos y cada uno de los datos que tenga allí almacenados.

DROP TABLE usuarios 


2. Manipulación de datos. 

SELECT. Consulta y trae información de nuestras tablas. 
Nos permite traer todos los registros de una tabla específica junto con todas las columnas
de cada uno o utilizar el comodín * para seleccionar todos los campos

SELECT * FROM entidadSELECT campo1, campo2, campo3 FROM entidad 


INSERT. Introduce nuevos registros en nuestra tabla. 
Debemos introducir entre paréntesis y separado por coma todos los campos donde deseo insertar un dato, 
luego la palabra reserva VALUES y nuevamente entre paréntesis los valores a insertar

INSERT INTO usuarios (nombre, apellido, edad) VALUES (“Gustavo”, “Fernandez”, 25);


UPDATE. Actualiza uno o varios registros en nuestra tabla.

UPDATE usuarios SET nombre = “Marcos”, apellido=”Sandoval”


DELETE. Elimina registros en nuestra tabla.

DELETE FROM usuarios


WHERE. La palabra reservada WHERE nos permite añadir condiciones a nuestros sentencias SQL para seleccionar registros según la condición que escribamos. 
Podemos utilizar WHERE combinado con el

SELECT, UPDATE o DELETE.SELECT * FROM usuarios WHERE

 */