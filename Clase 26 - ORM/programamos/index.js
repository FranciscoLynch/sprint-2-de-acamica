const express = require('express');
const app = express();
const csv = require('csvtojson');

// CONFIGURO SEQUELIZE
const { Sequelize, DataTypes, Model, Op } = require('sequelize');
const sequelize = new Sequelize('acamicadb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

sequelize.authenticate().then(() => {
    console.log('Conexion establecida');
}).catch(() => {
    console.log('No se ha podido conectar a la base de datos');
});

// SE CREA EL MODELO DE LA TABLA "MARCA"
class Marca extends Model { };

Marca.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre: DataTypes.STRING,
}, {
    sequelize,
    modelName: 'marca',
    timestamps: false
});

// SE CREA EL MODELO DE LA TABLA MODELO
class Modelo extends Model { };

Modelo.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre: DataTypes.STRING,
}, {
    sequelize,
    modelName: 'modelo',
    timestamps: false
});

// SE CREA EL MODELO DE LA TABLA TELEVISOR
class Televisor extends Model { };

Televisor.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    pantalla: DataTypes.INTEGER,
    smart: DataTypes.BOOLEAN,
    precio: DataTypes.INTEGER
},
    {
        sequelize,
        modelName: 'televisor',
        timestamps: false
    });

// SE CREA LA RELACION, LA LLAVE FORANEA DE LA TABLA MARCA EN LA TABLA MODELO
Televisor.belongsTo(Marca, { foreignKey: 'id_marca' });
Televisor.belongsTo(Modelo, { foreignKey: 'id_modelo' });


// INGRESO DATOS A LA TABLA MARCA
(async () => {
    await sequelize.sync({ force: true });
    await Marca.create({
        nombre: 'LG'
    })
        .then(console.log('Se han ingresado los datos de la marca'))
        .catch(console.log('Ha ocurrido un error al intentar ingresar los datos de la la marca'));
})();

// INGRESO DATOS A LA TABLA MODELO
(async () => {
    await sequelize.sync({ force: true });
    await Modelo.create({

    })
        .then(console.log('Se han ingresado los datos del modelo'))
        .catch(console.log('Ha ocurrido un error al intentar ingresar los datos del modelo'));
})();

// INGRESO DATOS A LA TABLA TELEVISOR
(async () => {
    await sequelize.sync({ force: true });
    await Televisor.create({
        id_marca: 1,
        id_modelo: 1,
        pantalla: 32,
        smart: true,
        precio: 30000,
    })
        .then(console.log('Se han ingresado los datos del televisor'))
        .catch(console.log('Ha ocurrido un error al intentar ingresar los datos del televisor'));
})();

// OBTENER LA INFORMACION DE TODOS LOS TELEVISORES
app.get('/', async (req, res) => {
    const televisores = await Televisor.findAll();
    res.json(televisores);
});

// AGREGAR UNA MARCA 
app.post('/marca', (req, res) => {
    const marca = await Marca.create(req.body);
    res.json(marca);
});

// AGREGAR UN MODELO
app.post('/modelo', (req, res) => {
    const modelo = await Modelo.create(req.body);
    res.json(modelo);
});

// AGREGAR UN TELEVISOR
app.post('/', async (req, res) => {
    const televisor = await Televisor.create(req.body);
    res.json(televisor);
});

// PRECIOS // SE INGRESA UN PARAMETRO EN CADA ENDPOINT PARA LIMITAR Y DEVOLVER CIERTOS PRECIOS 
// EN ESTE CASO, A PARTIR DEL PARAMETRO(PRECIO) INGRESADO MUESTRA LOS MAS ALTOS 

app.get('/preciosMasAltos/:p', (req, res) => {

    const precios = await Televisor.findAll({
        where: {
            [Op.gt]: req.params.p,
        }
    });
    res.json(precios);

});

// EN ESTE, MUESTRA LOS MAS BAJOS
app.get('/preciosMasBajos/:p', (req, res) => {

    const precios = await Televisor.findAll({
        where: {
            [Op.lt]: req.params.p,
        }
    });
    res.json(precios);

});

// TODOS LOS TELEVISORES DE UNA MARCA 
app.get('/marca/:marca', (req, res) => {
    const marca = req.params.marca;
    const x = await Televisor.findAll({
        where: {
            id_marca: marca
        }
    });
    res.json(x);
});

// ORDERING
// ORDENAR Y MOSTRAR LOS TELEVISORES DE MENOR A MAYOR PRECIO
app.get('/orderBy', (req, res) => {
    const ordered = await Televisor.findAll({
        order: ['precio', 'DESC']
    });
    res.json(ordered);
});


app.listen(5000, () => console.log('SERVER ON'))