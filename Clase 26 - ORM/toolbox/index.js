const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('acamicadb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

class User extends Model { }

// El modelo
User.init({
    nombre: DataTypes.STRING,   // campo nombre de la BD
    apellido: DataTypes.STRING  // campo apellido de la BD
}, { sequelize, modelName: 'users' });  /* Tabla users */

// Se ingresan los datos y se crea la tabla                 // POST 
(async () => {
    await sequelize.sync();
    const got = await User.create({
        nombre: 'Jon',
        apellido: 'Snow'
    });
    console.log(got.toJSON);
})();


// Usando el objeto got creado previamente                   // PUT 
// cambiamos el nombre y apellido 

/* 
        Codigo de la toolbox 
    // Usando el objeto got creado previamente
    
    console.log(got.nombre); // Jon
    console.log(got.apellido); // Snow
    
    // Cambiamos el nombre y el apellido 
    
    got.nombre = 'Arya'; 
    got.apellido = 'Stark'; 
    
    // lo guardamos 

    await got.save(); 
    
    // El registro es guardado de la base de datos 

    console.log(got.nombre); // Arya 
    console.log(got.apellido); // Stark 
*/

// Codigo de la pag de sequelize Model Querying - Basics - Simple UPDATE queries
(async () => {
    await sequelize.sync()
    await User.update({ nombre: 'Arya', apellido: 'Stark' }, {
        where: {
            nombre: 'Jon'
        }
    });
})();


//                                                           // DELETE

/* 
    Codigo de la toolbox 

    // usando el objeto got creado previamente 

    console.log(got.nombre); // Arya 
    console.log(got.apellido); // Stark  

    // lo eliminamos 

    await got.destroy(); 

    // el registro es borrado de la base de datos
*/

// Codigo de la pag de sequelize Model Querying - Basics - Simple DELETE queries 

// eliminar con una condicion
(async () => {
    await sequelize.sync();
    await User.destroy({
        where: {
            nombre: 'Jon' // o Arya
        }
    });
})();

// Para eliminar todo se usa TRUNCATE 
(async () => {
    await sequelize.sync();
    await User.destroy({
        truncate: true
    });
})();


// BUSCAR                                                      //                                        

// Codigo de la toolbox

// buscar un solo resultado 

const arya = await User.findOne({
    where: {
        nombre: 'Arya'
    }
});
console.log(arya); // listo el objeto 

// buscar muchos resultados 

const starks = await User.findAll({
    where: {
        apellido: 'Stark'
    }
});
starks.forEach(stark => { // Al ser un forEach, cada vez que se repita va a entrar por parametro el documento con la condicion y se va a mostrar 
    console.log(stark); // listo todos los resultados
})



// JOINS 

class House extends Model {} 

House.init({ 
    nombre: DataTypes.STRING, // campo nombre de la BD
}, { sequelize, modelName: 'houses'}); // tabla houses 

// Indicamos que un User pertence a una Casa, con el campo id_casa como clave foránea 
User.belongsTo(House, {foreignKey: 'id_casa'}); 

let all_starks = await User.findAll({ 
    include: {
        model: House, 
        where: { 
            nombre: 'Stark'
        }
    }
}); 
