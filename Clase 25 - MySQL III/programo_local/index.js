const mysql = require('mysql2');
const csv = require('csvtojson');

// database credentials 
const connection = mysql.createConnection({
    host: 'localhost',
    username: 'OyDGYFUXs9',
    password: 'oWk9WtxLH3',
    database: 'OyDGYFUXs9'
});

connection.connect((err) => {
    if (err) return console.error(
        'error: ' + err.message
    );
    connection.query('DROP TABLE students',
        (err, drop) => {

            // Query to create table 'sample' 
            var createStatement =
                "CREATE TABLE students(Name char(50), " +
                "Email char(50), Age int, city char(30))"

            // Creating table 'sample' 
            connection.query(createStatement, (err, drop) => {
                if (err)
                    console.log('ERROR: ', err);
                console.log('Finalizado la creacion de tabla students')
            });
        });

});

// CSV file name 
const filename = "C:/Users/lynch/Desktop/PROGRAMACION/sprint-2-de-acamica/Clase 25 - MySQL III/programo_local/data.csv";

csv().fromFile(filename).then(source => {

    // Fetching the data from each row 
    // and inserting to the table 'sample' 
    for (var i = 0; i < source.length; i++) {
        var Name = source[i]["Name"],
            Email = source[i]["email"],
            Age = source[i]["Age"],
            City = source[i]["City"];

        var insertStatement = `INSERT INTO students values(?, ?, ?, ?)`;
        var items = [Name, Email, Age, City];

        // inserting data of current row 
        // into database 
        
        connection.query(insertStatement, items,
            (err, results, fields) => {
            if (err) {
                console.log("No se pudo insertar el registro en el item ", i + 1);
                return console.log(err);
            }
        });
    }
    console.log("Todos los items fueron almacenados satisfactoriamente");
});
