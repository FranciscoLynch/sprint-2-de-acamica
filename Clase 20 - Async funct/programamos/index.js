const { default: fetch } = require('node-fetch');

async function obtenerUsuario() {
    const response = await fetch("http://api.github.com/users/andrew");
    const data = await response.json();
    if (response.status === 200) {
        return data;
    } else {
        console.log(new Error("Se presento un error"));
    }
}

obtenerUsuario().then((data) => {
    console.log('la data es ', data);
}).catch((error) => {
    console.log(error);
});

async function obtenerPeliculas() {
    const response = await fetch("http://www.omdbapi.com/?t=angry&apikey=b795586a");
    const data = await response.json();
    if (data) {
        return data;
    } else {
        return "mensaje error";
    }
} 

obtenerPeliculas().then(data => console.log(data)); 



