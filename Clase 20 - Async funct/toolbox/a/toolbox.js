const { default: fetch } = require("node-fetch")

let hi = (async () => { 
    const response = await fetch('');
    const data = await response.json(); 
    if (response.status === 200){ 
        return data;
    } else { 
        return "Error";
    }
})().then((result) => {
    console.log(result);
}).catch((error) => {
    console.log(error);
})
