var div = document.getElementById('imgMovie'); 

async function obtenerPeliculas(){ 
    const response = await fetch("http://www.omdbapi.com/?t=angry birds&apikey=2ba95592"); 
    const data = await response.json(); 
    if (data){ 
        return data; 
    } else { 
        return "mensaje error"; 
    } 
} 

obtenerPeliculas().then((data) => { 
    //imagen
    let peliImage = document.createElement('img'); 
    peliImage.setAttribute('src',data.Poster);
    peliImage.style.width = '300px';
    //titulo 
    let peliTitulo = document.createElement('h1');
    peliTitulo.setAttribute('id', "titulo");
    peliTitulo.innerHTML = data.Title; 
    //descripcion 
    let peliDescrip = document.createElement('p');
    peliDescrip.setAttribute('id', 'Descripcion');
    peliDescrip.innerHTML = data.peliTitulo; 
    //añadimos al div 
    div.appendChild(peliImage); 
    div.appendChild(peliTitulo); 
    div.appendChild(peliDescrip); 
}).catch((error) => { 
    console.log(error);
});