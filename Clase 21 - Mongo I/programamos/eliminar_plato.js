const mongoose = require('./conexion.js');
const express = require('express');
const app = express();

app.listen(3000, () => {
    console.log('listening on 3000');
});

const platoSchema = mongoose.Schema({
    plato: String,
    precio: String,
    tipo_de_plato: String
});

const platos = mongoose.model('Platos', platoSchema);
try {
    platos.deleteOne({ _id: ' ' }).then((result, error) => {
        console.log('Plato eliminado', result); 
        if (error) { 
            throw new Error(error.message);
        }
    });
} catch (err) {
    console.log(err);
} 


app.delete('/eliminaplato', (req,res) => {
    try{ 
        platos.deleteOne({nombre: req.params.nombre}).then( (resultados, error) => {
            res.send('Se eliminaron exitosamente los datos');
            if (error) {
                throw new Error(error.message);
            }
        }); 

    } catch(err){ 
        res.send('Hubo un error:', err);
    }
});