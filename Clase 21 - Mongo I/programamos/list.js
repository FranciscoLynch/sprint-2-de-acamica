const mongoose = require('./conexion.js');
const express = require('express'); 
const app = express(); 

app.use(express.static(__dirname + 'conexion.js')); 

app.listen(3000, () => { 
    console.log('listening on 3000'); 
}); 

const platos = mongoose.model('platos', { 
    plato: String, 
    precio: String, 
    tipo_de_plato: String 
}); 

// listado 

platos.find().then((resultado) => { 
    console.log(resultado);
});