const mongoose = require('./conexion.js'); 
const express = require('express');
const app = express(); 

app.listen(3000, () => { 
    console.log('listening on 3000');
});  

const platoSchema = mongoose.Schema({
    plato: String,
    precio: String,
    tipo_de_plato: String
});

const platos = mongoose.model('Platos', platoSchema);
try {
    platos.findOne({ _id: ' ' }).then((result, error) => {
        console.log('Plato encontrado', result); 
        if (error) { 
            throw new Error(error.message);
        }
    });
} catch (err) {
    console.log(err);
}