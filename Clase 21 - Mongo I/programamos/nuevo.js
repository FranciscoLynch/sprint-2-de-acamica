const mongoose = require('./conexion.js'); 
const express = require('express');
const app = express(); 

app.listen(3000, () => { 
    console.log('listening on 3000');
}); 

const platos = mongoose.model('platos', { 
   plato: String, 
   precio: String, 
   tipo_de_plato: String  
}); 

let nuevoPlato = { 
    plato: 'frutillas', 
    precio: '123$', 

    tipo_de_plato: 'postre'
} 
const nuevaOpcionPlato = new platos(nuevoPlato); 
nuevaOpcionPlato.save();