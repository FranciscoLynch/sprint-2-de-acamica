const mongoose = require('mongoose');
const app = require('express');
const express = express();
mongoose.connect('mongodb://localhost:3000/mi_base');

// SE DEFINE UN ESQUEMA
schema = { nombre: String, apellido: String, edad: Number }

// SE CREA UN MODELO CON EL ESQUEMA
const Usuarios = mongoose.model("Usuarios", schema);

// DEFINE UN OBJETO CON EL DATO QUE QUEREMOS ALMACENAR Y GUARDAMOS A TRAVES DEL MODELO
const yo = { nombre: "Juan", apellido: "Perez", edad: 24 };
let nuevo_usuario = new Usuarios(yo)
nuevo_usuario.save();

// BUSCAR INFORMACION
Usuarios.find({ nombre: "Juan" }).then(function (resultados) {
    console.log(resultados);
});